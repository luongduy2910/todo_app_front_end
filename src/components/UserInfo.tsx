import { useQueryUser } from '@/hooks/useQueryUser'
import { Loader } from '@mantine/core';
import React from 'react'

export const UserInfo = () => {
    let {data : user , status} = useQueryUser() ; 
    if(status === 'loading') return <Loader/>
    return (
        <p>{user?.email}</p>
    )
}
