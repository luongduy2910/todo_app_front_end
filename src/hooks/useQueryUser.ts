import { User } from '@prisma/client'
import { useQuery } from '@tanstack/react-query'
import { data } from 'autoprefixer'
import axios from 'axios'
import { useRouter } from 'next/router'
import React from 'react'

export const useQueryUser = () => {
    const router = useRouter()
    const getUser = async () => {
        const {data} = await axios.get<Omit<User , 'hashedpassword'>>(`${process.env.NEXT_PUBLIC_API_URL}/user`)
        return data
    }
    return useQuery<Omit<User , 'hashedpassword'>>({
        queryKey : ['user'] , 
        queryFn : getUser, 
        onError : (err : any) => {
            if(err.response.status === 401 || err.response.status === 403)
            router.push('/') ; 
        }
    })
}
